const bcrypt = require('bcrypt');


module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: {
            allowNull: false,
            type: DataTypes.STRING(120),
            validate: {
                len: [0, 120]
            }
        },
        email: {
            allowNull: false,
            unique: true,
            type: DataTypes.STRING,
            validate: {
                isEmail: {
                    msg: 'Invalid email format.'
                }
            }
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING
        }
    }, {
        hooks: {
            beforeCreate: (obj, opts) => {

                // Encrypt the password before create object
                // on database.
                obj.password = bcrypt.hashSync(obj.password, 10);

            }
        }
    });

    User.associate = function(models) {
        // associations can be defined here
    };


    /**
     * Check if record exists based on a query result.
     * @param query
     * @returns {Promise<boolean>}
     */
    User.exists = async query => {
        const count = await User.count(query);

        // Consider that exists if the number
        // of query result is different
        // of zero.
        return count !== 0;
    };

    return User;
};