module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [0, 100]
      }
    },
    closedAt: DataTypes.DATE,
    description: DataTypes.TEXT,
    closed: DataTypes.BOOLEAN
  }, {});

  Task.associate = models => {

    // - define foreign key to user.
    Task.belongsTo(models.User, {
      foreignKey: {name: 'userId'},
      as: 'user'
    });

  };

  return Task;
};