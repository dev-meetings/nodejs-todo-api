const express = require('express');
const { TokenAuthentication } = require('./utils/authentication');
const { IsAuthenticated } = require('./utils/permissions');
const { DefaultController } = require('./utils/controller');

const app = express();


// - App server definition

app.use(express.json());

app.use(DefaultController({
    authenticators: [TokenAuthentication],
    permissions: [IsAuthenticated]
}));


// - Routers register

app.use('/api/v1/', require('./controllers/auth'));
app.use('/api/v1/hello/', require('./controllers/hello_world'));
app.use('/api/v1/tasks/', require('./controllers/tasks'));


// - Export App

module.exports = app;
