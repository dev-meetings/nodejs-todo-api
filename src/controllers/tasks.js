const express = require('express');

const parser = require('../utils/parser');
const { Task } = require('../models/index');
const { ApiController } = require('../utils/controller');
const { paginate } = require('../utils/pagination');

const router = express.Router();


router.get('/', ApiController({
    controller: async (request, response) => {
        // define the base query.
        let query = {where: {userId: request.user.id}};

        // apply closed filter.
        const closed = parser.parseBool(request.query.closed);

        if (closed !== null) { query.where.closed = closed; }

        // paginate queryset.
        await paginate(request, response, {model: Task, query: query});
    }
}));


router.post('/', ApiController({
    controller: async (request, response) => {
        let data = request.body;

        data.userId = request.user.id;

        // create task on database
        const obj = await Task.create(data);

        // send response.
        response.status(201).send(obj);
    }
}));


router.get('/:id', ApiController({
    controller: async (request, response) => {
        const id = request.params.id;

        const task = await Task.findOne({
            where: {
                id: id,
                userId: request.user.id
            }
        });

        if (!task) {
            response.status(404).send({
                'detail': 'Task not found!'
            });
        }

        response.status(200).send(task);
    }
}));


router.put('/:id', ApiController({
    controller: async (request, response) => {
        const id = request.params.id;

        const task = await Task.findOne({
            where: {
                id: id,
                userId: request.user.id
            }
        });

        if (!task) {
            response.status(404).send({
                'detail': 'Task not found!'
            });
        }

        await task.update({
            title: request.body.title,
            description: request.body.description,
            closed: request.body.closed,
            closedAt: request.body.closedAt
        });

        return response.status(200).send(task);
    }
}));


router.delete('/:id', ApiController({
    controller: async (request, response) => {
        const id = request.params.id;

        const task = await Task.findOne({
            where: {
                id: id,
                userId: request.user.id
            }
        });

        if (!task) {
            response.status(404).send({
                'detail': 'Task not found!'
            });
        }

        await task.destroy();

        return response.status(204).send();
    }
}));


module.exports = router;