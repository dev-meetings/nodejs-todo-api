const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const express = require('express');

const db = require('../models/index');
const settings = require('../config');
const { check } = require('express-validator/check');
const { ValidationHandler } = require('../utils/validation');
const { ApiController } = require('../utils/controller');

const router = express.Router();

/**
 * @api {post} /api/v1/sign-up/ Sign Up
 * @apiName Sign Up
 *
 * @apiSuccess (201) {Object} `User` object
 */
router.post('/sign-up', [
    check('name')
        .exists().withMessage('Name is required.').trim(),
    check('password1')
        .exists().withMessage('Password1 is required.')
        .isLength({min: 8}).withMessage('Password must be at least 8 chars.')
        .custom((password1, {req}) => {
            if (password1 !== req.body.password2) {
                throw new Error('Password does not match.');
            }
            return password1;
        }),
    check('email')
        .isEmail().trim().custom(async (email, {req}) => {
        email = email.toLowerCase();

        if (await db.User.exists({where: {email}})) {
            throw new Error(`User with email '${email}' already exists.`);
        }

        return email;
    }),
], ValidationHandler, async (request, response) => {
    // - Create user on database
    const user = await db.User.create({
        name: request.body.name,
        email: request.body.email,
        password: request.body.password1
    });

    // - Send response.
    response.status(201).send({
        id: user.id,
        name: user.name,
        email: user.email,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt
    });
});


router.post('/login', [
    check('email')
        .exists().withMessage('Email is required.')
        .isEmail().withMessage('Email is not valid.'),
    check('password')
        .exists().withMessage('Password is required.')
], ValidationHandler, async (request, response) => {
    const {email, password} = request.body;

    const user = await db.User.findOne({
        where: {email}
    });

    if (!bcrypt.compareSync(password, user.password)) {
        return response.status(400).send([
            {'email': 'Invalid email or password.'}
        ])
    }

    const token = jwt.sign({id: user.id}, settings.secret, {expiresIn: 3600});

    response.send({
        id: user.id,
        name: user.name,
        email: user.email,
        token: token,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt
    });
});


router.get('/test', ApiController({
    controller: (request, response) => {
        response.send(request.user);
    }
}));


/**
 * @api {post} /api/user List Users
 * @apiName List Users
 *
 * @apiSuccess (200) {Array} `User`
 */
// controllers.list = async (request, response) => {
//
//     let users = {};
//
//     try {
//         users = await paginate({
//             model: db.User,
//             query: {
//                 where: {
//                     email: 'juninho@test.com'
//                 }
//             },
//             page: request.query.page || 1,
//             perPage: 3
//         });
//     } catch (exc) {
//         if (exc instanceof PaginationError) {
//             response.status(404).send({details: exc.message})
//         }
//     }
//
//     response.status(200).send(users);
// };


module.exports = router;