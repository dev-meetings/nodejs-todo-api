const express = require('express');

const router = express.Router();


const PI = 3.14;


router.get('/', (request, response, next) => {
    console.log('Step 1;');
    request.pi = PI;
    next();
  }, (request, response, next) => {
    console.log(`Step 2; ${request.pi}`);
    next();
  },
  (request, response) => {
    console.log('Final Step;');
    response.status(200).send({
        message: 'Hello World'
    })

  });


module.exports = router;
