const { validationResult } = require('express-validator/check');

/**
 * A middleware that grants that the data will be
 * validated before execute the route.
 * @param request
 * @param response
 * @param next
 */
module.exports.ValidationHandler = (request, response, next) => {
    let errors = validationResult(request).formatWith(({ param, msg }) => {
        let error = {};
        error[param] = msg;
        return error;
    });

    if (!errors.isEmpty()) {
        return response.status(400).send(errors.array());
    }

    next();

};