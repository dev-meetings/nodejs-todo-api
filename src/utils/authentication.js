const jwt = require('jsonwebtoken');
const db = require('../models/index');
const settings = require('../config');


/**
 * Simple token authentication that uses jwt token
 * to authorize the access.
 */
module.exports.TokenAuthentication = {

    /**
     * Authenticate the user.
     * @param request
     * @param response
     */
    authenticate: async (request, response) => {

        // get the authorization information from request.
        const authorization = (request.headers.authorization || '').split(' ');

        if (authorization.length !== 2 && authorization[0].toLowerCase() !== 'token') {
            return null;
        }

        // get user token.
        const token = authorization[1];

        try {

            // decode the token.
            const decoded = jwt.verify(authorization[1], settings.secret);

            // find a user using decoded information.
            const user = await db.User.findOne({where: {id: decoded.id}});

            if (!user) {
                return null;
            }

            return [user, token];

        } catch (e) {

            return null;

        }

    }

};
