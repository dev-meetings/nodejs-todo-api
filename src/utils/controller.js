module.exports.ApiController = (opts) => {

    opts = opts || {};

    // get controller extra middleware.
    const middleware = opts.middleware || [];

    // init a pipeline for middleware.
    let pipeline = [];

    // a middleware to perform authentication.
    pipeline.push(async (request, response, next) => {

        const authenticators = opts.authenticators || request.controller.defaults.authenticators;

        request.isAuthenticated = false;

        for (let i = 0; i < authenticators.length; i++) {

            // get the authenticator.
            const authenticator = authenticators[i];

            // perform the authentication.
            const credentials = await authenticator.authenticate(request, response);

            if (credentials) {

                // set the credentials information to request.
                request.user = credentials[0];
                request.isAuthenticated = true;

                break;

            }
        }

        next();

    });

    // a middleware that checks all permissions in a view.
    pipeline.push(async (request, response, next) => {

        const permissions = opts.permissions || request.controller.defaults.permissions;

        for (let i = 0; i < permissions.length; i++) {
            const permission = permissions[i];
            const hasPerms = await permission.check(request, response);

            if (!hasPerms) {
                return response.status(permission.code || 403).send({
                    'detail': permission.message || 'Forbidden',
                });
            }
        }

        next();

    });

    // register extra middleware.
    for (let i = 0; i < middleware.length; i++) {
        pipeline.push(middleware[i]);
    }

    // register the controller.
    pipeline.push(opts.controller);

    // return the pipeline.
    return pipeline;

};


module.exports.DefaultController = opts => {

    return async (request, response, next) => {

        request.controller = {
            defaults: {
                authenticators: opts.authenticators || [],
                permissions: opts.permissions || []
            }
        };

        next();

    };

};