/**
 * Parse a string value to boolean.
 *
 * @param value: string.
 * @returns {boolean, null}
 */
module.exports.parseBool = (value) => {

    if (!value) {
        return null;
    }

    if (value.toLowerCase() === 'true' || value.toLowerCase() === 'false') {
        return value.toLowerCase() === 'true';
    }

    throw new Error(`The value ${value} is not a boolean.`);

};