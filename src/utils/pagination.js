/**
 * A simple way to paginate a queryset using sequelize ORM.
 * @param request
 * @param response
 * @param options
 * @throws PaginationError
 */
exports.paginate = async (request, response, options) => {
    const model = options.model;

    // - set the default page size;
    const perPage = (options.perPage || 10);

    // - set the default page number;
    let pageNumber = parseInt(request.query.page || 1);
    pageNumber = (pageNumber <= 0 ? 1 : pageNumber);

    // - define the results
    let results = {
        count: 0,
        next: null,
        previous: null,
        results: []
    };

    // - set an empty query if undefined;
    let query = options.query || {};

    // - count all entries for provided query;
    results.count = await model.count(query);

    if (results.count === 0) {
        // - accept empty queryset.
        return response.status(200).send(results);
    }

    // - count how many pages to query;
    let pages = Math.ceil(results.count / perPage);

    if (pageNumber > pages) {
        // in case of the page does not exist for provided queryset,
        // do not continue.
        return response.status(404).send({detail: `Page ${pageNumber} is out of range.`});
    }

    results.previous = (pageNumber === 1 ? null : pageNumber - 1); // get the previous page number.
    results.next = (pageNumber === pages ? null : pageNumber + 1); // get the next page number.

    // - get a paginated parameters to query.
    query.limit = perPage;                      // set limit to query.
    query.offset = perPage * (pageNumber - 1);  // set offset to query.

    // - get the queryset.
    results.results = await model.findAll(query);

    return response.status(200).send(results)
};