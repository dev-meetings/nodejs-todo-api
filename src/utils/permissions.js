/**
 * Check if user is authenticated.
 */
module.exports.IsAuthenticated = {
    message: 'User credentials was not provided or are invalid.',
    code: 403,
    check: async (request, response) => {
        return request.isAuthenticated === true;
    }
};


module.exports.IsCustomer = {
    check: async (request, response) => {
        return request.user.role === 'CUSTOMER';
    }
}
