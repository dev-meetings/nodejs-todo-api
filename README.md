# Todo API - NodeJS

I've developed this project to train and test NodeJS. That's why this project is not in both the best organization and patterns.

## Running the project

To run this project follow the steps:

1. First of all, clone the repository:

    ```bash
    $ git clone git@gitlab.com:dev-meetings/nodejs-todo-api.git
    ```

2. Install the project requirements.

    ```bash
    $ npm install
    ```

3. Install `nodemon` to run the project.

    ```bash
    $ sudo npm install -g nodemon
    ```

4. After that, access the project folder and start the database through `docker-compose`.

    ```bash
    $ docker-compose up -d db
    ```    

5. Run the migrations.

    ```bash
    $ sequelize db:migrate
    ```

6. Run the project.

    ```bash
    $ nodemon bin/server.js
    ```

IMPORTANT: I am considering that you already have the `nodejs`, `npm`, `docker` and `docker-compose` installed on your computer, if you don't install first.
