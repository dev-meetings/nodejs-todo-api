const app = require('../src/app.js');

// Starts the Server
//
//## Start to listen the server at port 3000

app.listen(3000, () => {
    console.log('The server has been started!');
});
